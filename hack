#!/bin/bash

# Check if user is root
if [ "$EUID" -ne 0 ]; then
	echo "Please run as root."
	exit
fi

###############################################################################

# Get OS
export platform='unknown'
export system_name=`uname`
if [[ "$system_name" == 'Linux' ]]; then
	platform='linux'
elif [[ "$system_name" == 'Darwin' ]]; then
	platform='osx'
fi

###############################################################################

# Load Complete
dialog --backtitle "GitHub Contribution Hacker" --infobox "LOADED." 3 20
sleep 1.25

###############################################################################

# Initializing Contribution Increase Sequence
export hacker_github_name=""
export hacker_email=""

exec 3>&1

dialog --separate-widget $'\n' --ok-label "Submit" \
	--backtitle "GitHub Contribution Hacker" \
	--title "INITIALIZING CONTRIBUTION HACK SEQUENCE" \
	--form "\nEnter GitHub information:\nUse arrows or click to move" \
	12 50 0 \
	"Username:" 1 1 "$hacker_github_name" 1 10 20 0 \
	"Email:" 2 1 "$hacker_email" 2 10 40 0 \
2>&1 1>&3 |{
	read -r hacker_github_name
	read -r hacker_email
	exitcode=$?

	if [ "$exitcode" = 0 ]; then
		git config --global credential.helper cache
		git config --global credential.helper 'cache --timeout=600'
		git config --global user.email "$hacker_email"
		git config --global user.name "$hacker_github_name"
		git config --global push.default simple

	else
		echo
		echo "Aborted"
		exit
	fi
}
exec 3>&-
    
###############################################################################

# Initalizing Time Portal Screens
dialog --backtitle "GitHub Contribution Hacker" \
	--infobox "INITIALIZING TIME PORTAL." 3 31
sleep .9

dialog --backtitle "GitHub Contribution Hacker" \
	--infobox "INITIALIZING TIME PORTAL.." 3 31
sleep .9

dialog --backtitle "GitHub Contribution Hacker" \
	--infobox "INITIALIZING TIME PORTAL..." 3 31
sleep .9

###############################################################################

# Calender input
date_picked=$(dialog --stdout --title "TIME PORTAL" \
		--backtitle "GitHub Contribution Hacker" \
		--no-cancel \
		--date-format '%Y%m%d' \
		--calendar "\nSelect a date to hack\nback in time to and wait for 5 seconds:" \
		0 0)
                 
if [ "$date_picked" != "" ]; then

    if [ "$platform" = "linux" ]; then
		d1=$(date -d "00:00" +%s)
		d2=$(date -d $date_picked +%s)
	elif [ "$platform" = "osx" ]; then
		d1=$(date -j -f "%a %b %d %T %Z %Y" "`date`" "+%s")
		d2=$(date -j -f "%Y%m%d" "$date_picked" "+%s")
	else
		d1=$(date -d "00:00" +%s)
		d2=$(date -d $date_picked +%s)
	fi
	
	days_diff=$(( ($d1-$d2) / 86400 ))

else
    echo
    echo "Aborted"
	exit
fi

###############################################################################

# Committing
daysHacked=0
until [ "$daysHacked" -eq "$days_diff" ]
do
	FLIP=$(($RANDOM%10))
	
	if [ $FLIP -ge 2 ]; then
		if [ "$platform" = "linux" ]; then
    	d=$(date -d "$date_picked + $daysHacked days" +%b" "%d" 00:00 "%Y)

		elif [ "$platform" = "osx" ]; then
	d=$(date -j -f "%a %b %d %T %Z %Y" "`date -v +"$daysHacked""d"`" \
	"+%b %d 00:00 %Y")

		else
	d=$(date -d "$date_picked + $daysHacked days" +%b" "%d" 00:00 "%Y)
	fi
	git commit --quiet --allow-empty --date="$d" -m "Hacked Time"
	
	if [ $FLIP -ge 4 ]; then 
	git commit --quiet --allow-empty --date="$d" -m "Hacked Contribution"
	git commit --quiet --allow-empty --date="$d" -m "Hacked Contribution"
	
			if [ $FLIP -ge 8 ]; then
	git commit --quiet --allow-empty --date="$d" -m "Hacked Contribution"
	git commit --quiet --allow-empty --date="$d" -m "Hacked Contribution"
	git commit --quiet --allow-empty --date="$d" -m "Hacked Contribution"
	
				if [ $FLIP = 9 ]; then 
	git commit --quiet --allow-empty --date="$d" -m "Hacked Contribution"
	git commit --quiet --allow-empty --date="$d" -m "Hacked Contribution"

				fi
			fi
		fi
	fi
((daysHacked++))
done

###############################################################################

COUNT=10
(
while test $COUNT != 110
do
	echo $COUNT
	echo "XXX"
	echo "INCREASING CONTRIBUTION"
	echo "XXX"
	COUNT=`expr $COUNT + 10`
	sleep .5
done
) | dialog --title "INCREASING CONTRIBUTION" --gauge "" 8 70 0

###############################################################################

dialog --backtitle "GitHub Contribution Hacker" \
	--infobox "INCREASING CONTRIBUTION" 3 31
sleep .9

dialog --backtitle "GitHub Contribution Hacker" \
	--infobox " " 3 31
sleep .9

dialog --backtitle "GitHub Contribution Hacker" \
	--infobox "INCREASING CONTRIBUTION" 3 31
sleep .9

dialog --backtitle "GitHub Contribution Hacker" \
	--infobox " " 3 31
sleep .9
dialog --backtitle "GitHub Contribution Hacker" \
	--infobox "INCREASING CONTRIBUTION" 3 31
sleep .9

###############################################################################

dialog --backtitle "GitHub Contribution Hacker" \
	--title "LAST STAGE" \
	--yesno "\nYOU'RE ABOUT\nTO INCREASE CONTRIBUTIONS,\nARE YOU SURE?" 10 30
       
yn_response=$?
case $yn_response in
	0) git push;;
	1) echo "Aborted";;
	255) echo "Aborted";;
esac
