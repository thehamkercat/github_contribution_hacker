BEFORE

<a href="http://imgur.com/HVHSfoR"><img src="http://i.imgur.com/HVHSfoR.png" title="Before" /></a>

AFTER

<a href="http://imgur.com/Whq44wj"><img src="http://i.imgur.com/Whq44wj.png" title="After" /></a>


A bash script that utilizes git --date to commit 
in the past. Effectivly commiting and increasing your contribution back in time. 



## How to run
#### Step 1
- Create new repo

#### Step 2
Make sure you have cURL, dialog, and git installed on your system

```bash
$ sudo apt-get update
$ sudo apt-get install curl
$ sudo apt-get install dialog
$ sudo apt-get install git
```

#### Step 3
Clone your new repository to your computer:

```bash
$ git clone https://github.com/Your_Username/Your_repo
$ cd ~/Your_repo/
```
Then Go [here](https://raw.githubusercontent.com/thehamkercat/github_contribution_hacker/master/hack), Copy everything, Then create a new file named `hack` in the folder which you cloned in the previous step and paste everything you just copied.

#### Step 4

```
$ sudo bash hack    # must run sudo to make sure your git is configured correctly
```
#### Warning
You can't just download the script from my repo and run it. You have to clone your own repo, create a new file and paste everything in that file and execute it.

## Contributing

#### Bug Reports & Feature Requests

Please use the [issue tracker](https://github.com/thehamkercat/github_contribution_hacker/issues) to report any bugs or file feature requests.

I'll be always on [telegram](https://t.me/thehamkercat)
